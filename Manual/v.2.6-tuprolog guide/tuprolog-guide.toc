\contentsline {chapter}{\numberline {1}What is \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{6}{chapter.1}
\contentsline {chapter}{\numberline {2}Installing \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Installation in Java}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Installation in .NET}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Installation in Android}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Installation in Eclipse}{12}{section.2.4}
\contentsline {chapter}{\numberline {3}Getting Started}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Prolog User}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Editing theories}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Solving goals}{18}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Debugging support}{21}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Dynamic library management}{21}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Java Developer}{23}{section.3.2}
\contentsline {section}{\numberline {3.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the .NET Developer}{30}{section.3.3}
\contentsline {section}{\numberline {3.4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Android User}{30}{section.3.4}
\contentsline {chapter}{\numberline {4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Basics}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Predicate categories}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Syntax}{36}{section.4.2}
\contentsline {section}{\numberline {4.3}Engine configurability}{38}{section.4.3}
\contentsline {section}{\numberline {4.4}Exception support}{39}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Error classification}{40}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Built-in predicates}{41}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Control management}{41}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Term unification and management}{43}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Knowledge base management}{45}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Operator and flag management}{47}{subsection.4.5.4}
\contentsline {subsection}{\numberline {4.5.5}Library management}{49}{subsection.4.5.5}
\contentsline {subsection}{\numberline {4.5.6}Directives}{50}{subsection.4.5.6}
\contentsline {chapter}{\numberline {5}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Libraries}{53}{chapter.5}
\contentsline {section}{\numberline {5.1}BasicLibrary}{55}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Predicates}{55}{subsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.1}Type Testing}{55}{subsubsection.5.1.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.2}Term Creation, Decomposition and Unification}{56}{subsubsection.5.1.1.2}
\contentsline {subsubsection}{\numberline {5.1.1.3}Occurs Check}{58}{subsubsection.5.1.1.3}
\contentsline {subsubsection}{\numberline {5.1.1.4}Expression and Term Comparison}{59}{subsubsection.5.1.1.4}
\contentsline {subsubsection}{\numberline {5.1.1.5}Finding Solutions}{59}{subsubsection.5.1.1.5}
\contentsline {subsubsection}{\numberline {5.1.1.6}Control Management}{60}{subsubsection.5.1.1.6}
\contentsline {subsubsection}{\numberline {5.1.1.7}Clause Retrieval, Creation and Destruction}{61}{subsubsection.5.1.1.7}
\contentsline {subsubsection}{\numberline {5.1.1.8}Operator Management}{62}{subsubsection.5.1.1.8}
\contentsline {subsubsection}{\numberline {5.1.1.9}Flag Management}{62}{subsubsection.5.1.1.9}
\contentsline {subsubsection}{\numberline {5.1.1.10}Actions on Theories and Engines}{63}{subsubsection.5.1.1.10}
\contentsline {subsubsection}{\numberline {5.1.1.11}Spy Events}{65}{subsubsection.5.1.1.11}
\contentsline {subsubsection}{\numberline {5.1.1.12}Auxiliary predicates}{65}{subsubsection.5.1.1.12}
\contentsline {subsection}{\numberline {5.1.2}Functors}{67}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Operators}{67}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}ISOLibrary}{70}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Predicates}{70}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}Type Testing}{70}{subsubsection.5.2.1.1}
\contentsline {subsubsection}{\numberline {5.2.1.2}Atoms Processing}{70}{subsubsection.5.2.1.2}
\contentsline {subsection}{\numberline {5.2.2}Functors}{72}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Operators}{73}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Flags}{73}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}IOLibrary}{74}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Predicates}{74}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}General I/O}{74}{subsubsection.5.3.1.1}
\contentsline {subsubsection}{\numberline {5.3.1.2}Helper Predicates}{79}{subsubsection.5.3.1.2}
\contentsline {subsubsection}{\numberline {5.3.1.3}Random Generation of Numbers}{81}{subsubsection.5.3.1.3}
\contentsline {section}{\numberline {5.4}DCGLibrary}{81}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Predicates}{82}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Operators}{83}{subsection.5.4.2}
\contentsline {section}{\numberline {5.5}ISOIOLibrary}{84}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Options}{86}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Writing terms}{89}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Predicates}{91}{subsection.5.5.3}
\contentsline {chapter}{\numberline {6}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Exceptions}{100}{chapter.6}
\contentsline {section}{\numberline {6.1}Exceptions in ISO Prolog}{100}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Error classification}{102}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Exceptions in \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{103}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Examples}{103}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Handling Java/.NET Exceptions from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{105}{subsection.6.2.2}
\contentsline {chapter}{\numberline {7}Multi-paradigm programming in Prolog and Java}{106}{chapter.7}
\contentsline {section}{\numberline {7.1}Using Java from Prolog: \textit {JavaLibrary}}{106}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Type mapping}{107}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Creating and accessing objects: an overview}{108}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}Examples}{112}{subsubsection.7.1.2.1}
\contentsline {subsubsection}{\numberline {7.1.2.2}Registering object bindings}{114}{subsubsection.7.1.2.2}
\contentsline {subsection}{\numberline {7.1.3}Predicates}{115}{subsection.7.1.3}
\contentsline {subsubsection}{\numberline {7.1.3.1}Object creation, class compilation and method invocation}{115}{subsubsection.7.1.3.1}
\contentsline {subsubsection}{\numberline {7.1.3.2}Array management}{118}{subsubsection.7.1.3.2}
\contentsline {subsubsection}{\numberline {7.1.3.3}Helper predicates}{120}{subsubsection.7.1.3.3}
\contentsline {subsection}{\numberline {7.1.4}Functors}{120}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}Operators}{120}{subsection.7.1.5}
\contentsline {subsection}{\numberline {7.1.6}Examples}{121}{subsection.7.1.6}
\contentsline {subsubsection}{\numberline {7.1.6.1}RMI Connection to a Remote Object}{121}{subsubsection.7.1.6.1}
\contentsline {subsubsection}{\numberline {7.1.6.2}A Swing GUI}{121}{subsubsection.7.1.6.2}
\contentsline {subsubsection}{\numberline {7.1.6.3}Database access via JDBC}{121}{subsubsection.7.1.6.3}
\contentsline {subsubsection}{\numberline {7.1.6.4}Dynamic compilation}{124}{subsubsection.7.1.6.4}
\contentsline {subsection}{\numberline {7.1.7}Handling Java Exceptions}{125}{subsection.7.1.7}
\contentsline {subsubsection}{\numberline {7.1.7.1}Java exception examples}{127}{subsubsection.7.1.7.1}
\contentsline {section}{\numberline {7.2}Using Prolog from Java: \textit {the Java API}}{131}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}A Taxonomy of Prolog types in Java}{131}{subsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.1.1}Further notes about \texttt {Term}s}{132}{subsubsection.7.2.1.1}
\contentsline {subsection}{\numberline {7.2.2}Prolog engines, theories and libraries}{134}{subsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.2.1}Further notes about \texttt {Prolog} engines}{134}{subsubsection.7.2.2.1}
\contentsline {subsection}{\numberline {7.2.3}Examples}{136}{subsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.3.1}Appending lists}{136}{subsubsection.7.2.3.1}
\contentsline {subsubsection}{\numberline {7.2.3.2}A console-based Prolog interpreter}{136}{subsubsection.7.2.3.2}
\contentsline {subsection}{\numberline {7.2.4}Registering object bindings}{139}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Capturing the Prolog output in Java}{141}{subsection.7.2.5}
\contentsline {section}{\numberline {7.3}Augmenting Prolog via Java:\\developing new libraries}{141}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Syntactic conventions}{143}{subsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.1.1}Capturing exceptions raised in libraries}{145}{subsubsection.7.3.1.1}
\contentsline {subsubsection}{\numberline {7.3.1.2}Capturing the Java output in Prolog}{145}{subsubsection.7.3.1.2}
\contentsline {subsubsection}{\numberline {7.3.1.3}Naming issues}{145}{subsubsection.7.3.1.3}
\contentsline {subsection}{\numberline {7.3.2}Hybrid Java+Prolog libraries}{149}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Library loading issues}{151}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}Library Name}{152}{subsection.7.3.4}
\contentsline {section}{\numberline {7.4}Augmenting Java via Prolog:\\the P@J framework}{152}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Term taxonomy}{154}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Examples}{155}{subsection.7.4.2}
\contentsline {chapter}{\numberline {8}Multi-paradigm programming in Prolog and .NET}{161}{chapter.8}
\contentsline {section}{\numberline {8.1}A bit of history}{161}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.1 and CSharpLibrary}{161}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.1.3: CSharpLibrary + exceptions}{162}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.2 and CLILibrary}{162}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}IKVM Basics}{164}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Dynamic vs. Static modality}{164}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Class loading issues}{165}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}The other way: writing .NET applications in Java}{166}{subsection.8.2.3}
\contentsline {section}{\numberline {8.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}.NET now}{168}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Highlights}{168}{subsection.8.3.1}
\contentsline {section}{\numberline {8.4}Using .NET from Prolog: OOLibrary}{170}{section.8.4}
\contentsline {subsubsection}{\numberline {8.4.0.1}Motivation}{170}{subsubsection.8.4.0.1}
\contentsline {subsubsection}{\numberline {8.4.0.2}Language Conventions}{171}{subsubsection.8.4.0.2}
\contentsline {subsubsection}{\numberline {8.4.0.3}OOLibrary: predicates}{172}{subsubsection.8.4.0.3}
\contentsline {subsection}{\numberline {8.4.1}Examples}{173}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}Handling .NET Exceptions}{177}{subsection.8.4.2}
\contentsline {section}{\numberline {8.5}Using Prolog from .NET: the API}{178}{section.8.5}
\contentsline {section}{\numberline {8.6}Augmenting Prolog via .NET:\\developing new libraries}{178}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Capturing exceptions raised in .NET libraries}{182}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Capturing the .NET output in Prolog}{182}{subsection.8.6.2}
\contentsline {section}{\numberline {8.7}Augmenting .NET via Prolog:\\the P@J framework revised}{184}{section.8.7}
\contentsline {section}{\numberline {8.8}Putting everything together}{184}{section.8.8}
\contentsline {subsection}{\numberline {8.8.1}Example: Multi-language TicTacToe}{186}{subsection.8.8.1}
\contentsline {subsection}{\numberline {8.8.2}Version 2.0}{191}{subsection.8.8.2}
\contentsline {subsection}{\numberline {8.8.3}From Version 2.0 to Version 2.0.1}{192}{subsection.8.8.3}
\contentsline {subsection}{\numberline {8.8.4}From Version 2.0.1 to Version 2.1}{194}{subsection.8.8.4}
\contentsline {subsection}{\numberline {8.8.5}From Version 2.1 to Version 2.2}{196}{subsection.8.8.5}
\contentsline {subsection}{\numberline {8.8.6}From Version 2.2 to Version 2.3.0}{197}{subsection.8.8.6}
\contentsline {subsection}{\numberline {8.8.7}From Version 2.3.0 to Version 2.3.1}{199}{subsection.8.8.7}
\contentsline {subsection}{\numberline {8.8.8}From Version 2.3.1 to Version 2.4}{199}{subsection.8.8.8}
\contentsline {subsection}{\numberline {8.8.9}From Version 2.4 to Version 2.5}{199}{subsection.8.8.9}
\contentsline {subsection}{\numberline {8.8.10}From Version 2.5 to Version 2.6}{199}{subsection.8.8.10}
\contentsline {section}{\numberline {8.9}Acknowledgments}{199}{section.8.9}
