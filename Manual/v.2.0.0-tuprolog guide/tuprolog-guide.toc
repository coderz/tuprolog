\contentsline {chapter}{\numberline {1}What is \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Installing \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Getting Started}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Prolog Programmer Quick Start}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Developer Quick Start}{10}{section.3.2}
\contentsline {chapter}{\numberline {4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Basics}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Structure of a \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Engine}{12}{section.4.1}
\contentsline {section}{\numberline {4.2}Prolog syntax}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Configuration of a \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Engine}{16}{section.4.3}
\contentsline {section}{\numberline {4.4}Built-in predicates}{17}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Control management}{17}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Term Unification and Management}{18}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Knowledge-base management}{18}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Operators and Flags Management}{19}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Libraries Management}{20}{subsection.4.4.5}
\contentsline {subsection}{\numberline {4.4.6}Directives}{20}{subsection.4.4.6}
\contentsline {chapter}{\numberline {5}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Libraries}{22}{chapter.5}
\contentsline {section}{\numberline {5.1}BasicLibrary}{24}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Predicates}{24}{subsection.5.1.1}
\contentsline {subsubsection}{Type Testing}{24}{section*.2}
\contentsline {subsubsection}{Term Creation, Decomposition and Unification}{25}{section*.3}
\contentsline {subsubsection}{Occurs Check}{26}{section*.4}
\contentsline {subsubsection}{Expression and Term Comparison}{26}{section*.5}
\contentsline {subsubsection}{Finding Solutions}{27}{section*.6}
\contentsline {subsubsection}{Control Management}{27}{section*.7}
\contentsline {subsubsection}{Clause Retrival, Creation and Destruction}{28}{section*.8}
\contentsline {subsubsection}{Operator Management}{29}{section*.9}
\contentsline {subsubsection}{Flag Management}{29}{section*.10}
\contentsline {subsubsection}{Actions on Theories and Engines}{29}{section*.11}
\contentsline {subsubsection}{Spy Events}{30}{section*.12}
\contentsline {subsubsection}{Auxiliary predicates}{31}{section*.13}
\contentsline {subsection}{\numberline {5.1.2}Functors}{32}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Operators}{32}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}ISOLibrary}{34}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Predicates}{34}{subsection.5.2.1}
\contentsline {subsubsection}{Type Testing}{34}{section*.14}
\contentsline {subsubsection}{Atoms Processing}{34}{section*.15}
\contentsline {subsection}{\numberline {5.2.2}Functors}{35}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Operators}{36}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Flags}{36}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}DCGLibrary}{37}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Predicates}{38}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Operators}{38}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}IOLibrary}{38}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Predicates}{39}{subsection.5.4.1}
\contentsline {subsubsection}{General I/O}{39}{section*.16}
\contentsline {subsubsection}{I/O and Theories Helpers}{41}{section*.17}
\contentsline {subsubsection}{Random Generation of Numbers}{41}{section*.18}
\contentsline {chapter}{\numberline {6}Accessing Java from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{43}{chapter.6}
\contentsline {section}{\numberline {6.1}Mapping data structures}{43}{section.6.1}
\contentsline {section}{\numberline {6.2}General predicates description}{43}{section.6.2}
\contentsline {section}{\numberline {6.3}Predicates}{49}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Method Invocation, Object and Class Creation}{49}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Java Array Management}{51}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Helper Predicates}{52}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Functors}{52}{section.6.4}
\contentsline {section}{\numberline {6.5}Operators}{52}{section.6.5}
\contentsline {section}{\numberline {6.6}Java Library Examples}{53}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}RMI Connection to a Remote Object}{53}{subsection.6.6.1}
\contentsline {subsection}{\numberline {6.6.2}Java Swing GUI from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{53}{subsection.6.6.2}
\contentsline {subsection}{\numberline {6.6.3}Database access via JDBC from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{54}{subsection.6.6.3}
\contentsline {subsection}{\numberline {6.6.4}Dynamic compilation}{55}{subsection.6.6.4}
\contentsline {chapter}{\numberline {7}The IDE}{58}{chapter.7}
\contentsline {section}{\numberline {7.1}Editing the theory}{60}{section.7.1}
\contentsline {section}{\numberline {7.2}Solving goals}{60}{section.7.2}
\contentsline {section}{\numberline {7.3}Debug Informations}{64}{section.7.3}
\contentsline {section}{\numberline {7.4}Dynamic library management}{64}{section.7.4}
\contentsline {chapter}{\numberline {8}Using \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} from Java}{70}{chapter.8}
\contentsline {section}{\numberline {8.1}Getting started}{70}{section.8.1}
\contentsline {section}{\numberline {8.2}Basic Data Structures}{71}{section.8.2}
\contentsline {section}{\numberline {8.3}Engine, Theories and Libraries}{74}{section.8.3}
\contentsline {section}{\numberline {8.4}Some more examples of \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} usage}{77}{section.8.4}
\contentsline {chapter}{\numberline {9}How to Develop New Libraries}{81}{chapter.9}
\contentsline {section}{\numberline {9.1}Implementation details}{81}{section.9.1}
\contentsline {section}{\numberline {9.2}Library Name}{83}{section.9.2}
\contentsline {chapter}{\numberline {10}Exceptions}{85}{chapter.10}
\contentsline {section}{\numberline {10.1}Exceptions in ISO Prolog}{85}{section.10.1}
\contentsline {subsection}{\numberline {10.1.1}Examples}{86}{subsection.10.1.1}
\contentsline {subsection}{\numberline {10.1.2}Error classification}{88}{subsection.10.1.2}
\contentsline {section}{\numberline {10.2}Implementing Exceptions in tuProlog}{90}{section.10.2}
\contentsline {subsection}{\numberline {10.2.1}Java exceptions from tuProlog}{91}{subsection.10.2.1}
\contentsline {subsection}{\numberline {10.2.2}Examples}{92}{subsection.10.2.2}
